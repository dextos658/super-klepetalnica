  var nadimek = [];
/**
 * Zamenjava kode smeška s sliko (iz oddaljenega strežnika
 * https://sandbox.lavbic.net/teaching/OIS/gradivo/{smesko}.png)
 * 
 * @param vhodnoBesedilo sporočilo, ki lahko vsebuje kodo smeška
 */
function dodajSmeske(vhodnoBesedilo) {
  var preslikovalnaTabela = {
    ";)": "wink.png",
    ":)": "smiley.png",
    "(y)": "like.png",
    ":*": "kiss.png",
    ":(": "sad.png"
  }
  for (var smesko in preslikovalnaTabela) {
    vhodnoBesedilo = vhodnoBesedilo.split(smesko).join(
      "<img src='https://sandbox.lavbic.net/teaching/OIS/gradivo/" +
      preslikovalnaTabela[smesko] + "' />");
  }
  return vhodnoBesedilo;
}

function dodajSlike(vhodnoBesedilo) {
  
}


/**
 * Čiščenje besedila sporočila z namenom onemogočanja XSS napadov
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementEnostavniTekst(sporocilo) {
  preim = sporocilo;
  
  if (preim.indexOf("se je preimenoval v") > -1) {
    preim = preim.split(" ");
    preim[5] = preim[5].substring(0, preim[5].length-1);
    nadimek[preim[5]] = nadimek[preim[0]];
  }
  
  var jeSmesko = sporocilo.indexOf("https://sandbox.lavbic.net/teaching/OIS/gradivo/") > -1;
  var besede = sporocilo.split(" ");
  var stevec = 0;
  for (var i = 0; i < besede.length; i++) {
    if (besede[i].startsWith("http") && (besede[i].endsWith(".jpg") || besede[i].endsWith(".png") || besede[i].endsWith(".gif") || besede[i].endsWith(".gif.")  || besede[i].endsWith(".png.")  || besede[i].endsWith(".jpg."))) {
      stevec++;
    }
   }
  
  
  if (jeSmesko || (stevec > 0) ) {
    sporocilo = 
      sporocilo.split("<").join("&lt;")
               .split(">").join("&gt;")
               .split("&lt;img").join("<img")
               .split("png' /&gt;").join("png' />");
               
     for (var i = 0; i < besede.length; i++) {
    
     if (besede[i].startsWith("http") && ((besede[i].endsWith(".jpg") || besede[i].endsWith(".png") || besede[i].endsWith(".gif")))) {
      var slika = "<br/><img src=" + besede[i] + " style='width:200px; padding-left:20px;'> ";
      // sporocilo = sporocilo + slika;
     } else if (besede[i].startsWith("http") && ((besede[i].endsWith(".jpg.") || besede[i].endsWith(".png.") || besede[i].endsWith(".gif.")))) {
      besede[i] = besede[i].substring(0, besede[i].length - 1);
      
      var slika = "<br/><img src=" + besede[i] + " style='width:200px; padding-left:20px;'> ";
      sporocilo = sporocilo + slika;
     }
   }
        
    return divElementHtmlTekst(sporocilo);
  } else {
    return $('<div style="font-weight: bold"></div>').text(sporocilo); 
  }
  

}


/**
 * Prikaz "varnih" sporočil, t.j. tistih, ki jih je generiral sistem
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementHtmlTekst(sporocilo) {
  
  var besede = sporocilo.split(" ");
  var stevec = 0;
  for (var i = 0; i < besede.length; i++) {
    if (besede[i].startsWith("http") && (besede[i].endsWith(".jpg") || besede[i].endsWith(".png") || besede[i].endsWith(".gif") || besede[i].endsWith(".gif.")  || besede[i].endsWith(".png.")  || besede[i].endsWith(".jpg."))) {
      stevec++;
    }
   }
   
   if ( (stevec > 0) ) {
               
     for (var i = 0; i < besede.length; i++) {
    
     if (besede[i].startsWith("http") && ((besede[i].endsWith(".jpg") || besede[i].endsWith(".png") || besede[i].endsWith(".gif")))) {
      var slika = "<br/><img src=" + besede[i] + " style='width:200px; padding-left:20px;'> ";
      sporocilo = sporocilo + slika;
     } else if (besede[i].startsWith("http") && ((besede[i].endsWith(".jpg.") || besede[i].endsWith(".png.") || besede[i].endsWith(".gif.")))) {
      besede[i] = besede[i].substring(0, besede[i].length - 1);
      
      var slika = "<br/><img src=" + besede[i] + " style='width:200px; padding-left:20px;'> ";
      sporocilo = sporocilo + slika;
     }
   }
        
    return $('<div></div>').html('<i>' + sporocilo + '</i>');
  } else {
    //return $('<div></div>').html('<i>' + sporocilo + '</i>'); 
  }
   
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

  var socket = io.connect();
  var trenutniVzdevek = "";
  var sprem1;
  var sprem2;
  var sprem3 = [];
  var preim;
  
/**
 * Obdelaj besedilo, ki ga uporabnik vnese v obrazec na spletni strani, kjer
 * je potrebno ugotoviti ali gre za ukaz ali za sporočilo na kanal
 * 
 * @param klepetApp objekt Klepet, ki nam olajša obvladovanje 
 *        funkcionalnosti uporabnika
 * @param socket socket WebSocket trenutno prijavljenega uporabnika
 */
function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  sporocilo = dodajSmeske(sporocilo);
  var sistemskoSporocilo;
  
  // Če uporabnik začne sporočilo z znakom '/', ga obravnavaj kot ukaz

  
  if (sporocilo.charAt(0) == '/') {
    
    var ukaz = sporocilo.substring(1,10);
    if (ukaz == "preimenuj") {
      
      sprem1 = sporocilo;
      sprem1 = sprem1.split(' ');
      sprem1.shift();
      sprem2 = sprem1.join(' ');
      sprem3 = sprem2.split('\"');
      
      if (sprem2[1] == trenutniVzdevek) ;
      else {
        nadimek[sprem3[1]] = sprem3[3];
      }
    }
    
    else {
    
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  // Če gre za sporočilo na kanal, ga posreduj vsem članom kanala
    }
  } else {
    sporocilo = filtirirajVulgarneBesede(sporocilo);
    
    klepetApp.posljiSporocilo(trenutniKanal, sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

// Branje vulgarnih besed v seznam
var vulgarneBesede = [];
$.get('/swearWords.txt', function(podatki) {
  vulgarneBesede = podatki.split('\r\n'); 
});


/**
* Iz podanega niza vse besede iz seznama vulgarnih besed zamenjaj z enako dolžino zvezdic (*).
* ,
* @param vhodni niz
*/
function filtirirajVulgarneBesede(vhod) {

 for (var i in vulgarneBesede) {
   var re = new RegExp('\\b' + vulgarneBesede[i] + '\\b', 'gi');
   vhod = vhod.replace(re, "*".repeat(vulgarneBesede[i].length));
 }
 
 return vhod;
}


var trenutniKanal = "";

// Poačakj, da se celotna stran naloži, šele nato začni z izvajanjem kode
$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  
  // Prikaži rezultat zahteve po spremembi vzdevka
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      trenutniVzdevek = rezultat.vzdevek;
      $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });
  
  // Prikaži rezultat zahteve po spremembi kanala
  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutniKanal = rezultat.kanal;
    $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });
  
  // Prikaži prejeto sporočilo
  socket.on('sporocilo', function (sporocilo) {
    var novElement = divElementEnostavniTekst(sporocilo.besedilo);
    $('#sporocila').append(novElement);
  });
  
  // Prikaži seznam kanalov, ki so na voljo
  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var i in kanali) {
      if (kanali[i] != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanali[i]));
      }
    }
  
    // Klik na ime kanala v seznamu kanalov zahteva pridružitev izbranemu kanalu
    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  // Prikaži seznam trenutnih uporabnikov na kanalu
  socket.on('uporabniki', function(uporabniki) {
    $('#seznam-uporabnikov').empty();
    
    var print = [];
    for (var i = 0; i < uporabniki.length; i++){
      if (nadimek.hasOwnProperty(uporabniki[i]) && nadimek[uporabniki[i]] != undefined) {
        print[i] = nadimek[uporabniki[i]] + " (" + uporabniki[i] + ")";
      }
      else {
        print[i] = uporabniki[i];
      }
    }
    
    for (var i=0; i < uporabniki.length; i++) {
      $('#seznam-uporabnikov').append(divElementEnostavniTekst(print[i]));
    }
    
   // Klik na ime uporabnika v seznamu uporabnikov pošlje krc
    $('#seznam-uporabnikov div').click(function() {
      var uporabnik = $(this).text();
      console.log(uporabnik);
      for (var i = 0; i < uporabnik.length; i++) {
        if (uporabnik[i] == "(") {
          var zacetek = 0;
          var konec = 0;

             for (var j = 0; j < uporabnik.length; j++) {
             if(uporabnik[j] == "(") zacetek = j;
              if(uporabnik[j] == ")") konec = j;
        }
         uporabnik = uporabnik.substring(zacetek+1, konec);
      
        }
      }
      

     
      console.log(uporabnik); 
      
      klepetApp.procesirajUkaz('/krcanje ' + uporabnik) + ' znak za KRC';
      //console.log("Link dela!");
      $('#poslji-sporocilo').focus();
      $('#sporocila').append(divElementHtmlTekst('(zasebno za ' + $(this).text() +'): &#9756;'));
    });
  
    
  });
  
  // Seznam kanalov in uporabnikov posodabljaj vsako sekundo
  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki', {kanal: trenutniKanal});
  }, 1000);

  $('#poslji-sporocilo').focus();
  
  // S klikom na gumb pošljemo sporočilo strežniku
  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});
